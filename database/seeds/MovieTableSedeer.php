<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Movie as Movie;
use Illuminate\Support\Facade\Hash;
class MovieTableSedeer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker=faker::create();
        $movie=new Movie;
        $movie->name=$faker->name;
        $movie->description='descripcion';
        $movie->user_id=1;
        $movie->state_id=1;
        $movie->save();
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Models\State; 
class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $state = array
        ([
        	'state'=>'activo'
        ],
        [
        	'state'=>'inactivo'
        ]
         );

        foreach ($state as $value) {
        	$state=new State;
        	$state->state=$value['state'];
        	$state->save();
        }
    }
}

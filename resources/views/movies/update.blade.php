
@extends('layouts.app')
@section('content')
<section class="container">
	<div class="row">
		<article class="Col-md-10 Col-md-offset-1">
			{!! Form::model($movie,['route' => ['movie.update',$movie->id], 'method' => 'put', 'novalidate']) !!}
			   <div class="form-group">
			   	<label>Name</label>
			   	<input type="text" name="name" required value="{{ $movie->name }}" class="form-control">
			   </div>
			   <div class="form-group">
			   	<label>Description</label>
			   	<input type="text" name="description" required value="{{ $movie->description }}" class="form-control">
			   </div>
			   <div class="form-group">
			   	<button type="submit" class="btn btn-success">Enviar</button>
			   </div>
			{!! Form::close() !!}
		</article>
	</div>
</section> 
@endsection
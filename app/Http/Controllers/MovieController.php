<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movie as Movie;

class MovieController extends Controller
{
    //mostrar
    public function edit($id)
    {
    $movie=Movie::find($id);
    return \View::make('movies/update',compact('movie'));
    }
    public function index(){

        
        $movies=Movie::all();
        return \View::make('movies/list',compact('movies'));
      
    }

    
	public function create()
	{
       return \View::make('movies/new');
	}

    // acciones
    public function show(Request $Request){
       $movies=Movie::where('name','like','%'.$Request->name.'%')->get();
       return \View::make('movies/list',compact('movies'));
    }
    public function update($id,Request $Request){
       $movie=Movie::find($id);
       $movie->name=$Request->name;
       $movie->description=$Request->description;
       $movie->save();
       return redirect('movie');
    }

    public function destroy($id){
        $movie=Movie::find($id);
        $movie->delete();
        return redirect()->back();
    }


    public function store(Request $Request)
    {
    	$movie=new Movie;
    	/*$movie->name=$Request->name;
    	$movie->description=$Request->description;
    	$movie->save();*/
    	$movie->create($Request->all());
    	return redirect('movie');

    }
}
